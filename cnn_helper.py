from tensorflow.keras import Sequential
from tensorflow.keras.layers import Dense
from zipfile import ZipFile
import tensorflow as tf
import tensorflow_hub as hub
import random
import os
import matplotlib.image as mimage
import matplotlib.pyplot as plt

class CNNhelper:
    """
    This class contains couple of methods that
    will help us when building cnn model
    or data preparation
    """
    __image_reschaled = False

    def download_zip_data(self, url):
        """
        download data and unzip it

        param:
            url (str) : file url
        """

        os.system("wget {}".format(url))

        # get file name in the end of url
        splited_url = url.split("/")

        # unzip the file
        with ZipFile(splited_url[-1]) as zip:
            zip.extractall()
            zip.close()

        return splited_url[-1]

    def visualize_random_img(self, path, target_class):
        """
        Visualize random image with target class

        args:
            path (str) : where you store the image
            target_class (str) : oh image path it should contain 1 or more folder that
                each folder represent 1 class of image
        return:
            metrices of image
        """

        path_img = os.path.join(path, target_class)

        # pick random sample
        picked_img = random.sample(os.listdir(path_img), 1)[0]

        # read image from path and file name
        imgs = mimage.imread(os.path.join(path_img, picked_img))

        # show the image
        plt.figure(figsize=(12, 8))
        plt.title(target_class)
        plt.imshow(imgs)
        plt.axis(False)

        return imgs

    def create_model_from_hub(self, model_url = "", num_classes=10, image_size=(224, 224), color_channel=3):
        """
        create sequential model from tensroflow hub

        args:
            url (str) : tensorflow model pre-trained
            num_classes (int): num of classes that you have
            image_size (tupple) : size of image e.g (128 x 128) -> (128, 128)
            color_channel (int) : color's channel e.g 3 for rgb and 1 for grayscale images
        return:
            uncompile model
        """

        feature_extraction = hub.KerasLayer(
            model_url,
            trainable=False,
            name="feature_extraction",
            input_shape=image_size+(color_channel,)
        )

        model = Sequential([
            feature_extraction,
            Dense(num_classes, activation="softmax")
        ])

        return model

    def plot_result(self, history):
        """
        create plot from model performance history

        args :
            history (tensorflow object history) -> history from trained model retuned by tensorflow model
        return :

        """

        plt.figure(figsize=(12, 8))
        plt.plot(history.history['accuracy'], label="accuracy")
        plt.plot(history.history['val_accuracy'], label="validation accuracy")
        plt.legend()
        plt.show()

        print("\n\n")

        plt.figure(figsize=(12, 8))
        plt.plot(history.history['loss'], label="loss")
        plt.plot(history.history['val_loss'], label="validation loss")
        plt.legend()
        plt.show()

    def load_custom_img(self, url, size=224, scale=False):
        """
        load custom image from file
        
        args:
            filename (str) : file's name
            size (str) : custom image size default (224, 224)
            scale (boolean) : if true image will scaled between 0 and 1 default False

        return:
            img (metrics of image)
        """

        filename = url.split("/")[-1]

        # download file
        os.system("wget {}".format(url))

        img = tf.io.read_file(filename)
        img = tf.image.decode_image(img)
        img = tf.image.resize(img, size=[size, size])

        # scale image between 0 and 1

        if scale:
            self.__image_reschaled = True
            img = img / 255.

        return img
    

    def predict(self, model, img_array, class_names):
        """
        run the prediction to model that you build

        args :
            model (object) : tensorflow model
            img_array (list) : list of image / metrices of image
            class_names (list) : list of class name / image label
        """

        # run the prediction
        prob = model.predict(tf.expand_dims(img_array, axis=0))
        pred = int(tf.argmax(prob, axis=1))

        # show result
        plt.figure(figsize=(12, 8))
        plt.title(class_names[pred] + " " + str(round(max(prob[0]) * 100, 2)))


        if not self.__image_reschaled:
            img_array = img_array / 255.
            
        plt.imshow(img_array)
        plt.axis(False)
        plt.show()