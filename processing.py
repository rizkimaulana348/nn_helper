from pathlib import Path
import shutil
import os

class Preprocessing:

    def create_folder(self, destination="", type="", class_name=[]):
        """
        create folder train and test and inside that create folder
        any class e.g flower cat car

        args:
            destination (str) -> where you want to create new folder
            type (str) -> train | test | validation
            class_name (list) -> list of classnames
        """

        ds_path = os.path.join(destination, type)

        for c in class_name:
            folder = os.path.join(ds_path, c)
            
            Path(folder).mkdir(parents=True, exist_ok=True)
            print("creating folder ---> %s"%(folder))

    def split_data(self, origin_path = "", destination_path = "", percentage = 70):
        """
        create training dataset e.g 70% percentage of total data

        args:
            percetange (int) : percentange of data that you want to store to train data
        """

        # check if destination folder is exists
        # if not create the folder

        if not os.path.exists(destination_path):
            print("Folder", destination_path, "belum ada, otomatis kami buat \n")
            Path(destination_path).mkdir(parents=True, exist_ok=True)

        # list all file on origin path directory
        files = os.listdir(origin_path)

        # calculate amount data from n% of precentile
        amount_data = round(len(files) / 100 * percentage)

        # iterate the first n amount_data and copy those file to
        # destination path

        for f in files[:amount_data]:
            shutil.copy(os.path.join(origin_path, f), os.path.join(destination_path, f))
            print("Memindahan file {} ---> {}".format(f, os.path.join(destination_path,f)))

        print("\n")
        print(amount_data, "file berhasil dipindahkan ke", destination_path, "\n\n")